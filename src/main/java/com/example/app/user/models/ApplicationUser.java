package com.example.app.user.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "user")
public class ApplicationUser {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "username", nullable = false, unique = true, length = 24)
    @NonNull
    private String username;

    @Column(name = "password", nullable = false, length = 60)
    @NonNull
    private String password;

    @ManyToOne
    @JoinColumn(name = "role", nullable = false)
    private UserRole role;
}