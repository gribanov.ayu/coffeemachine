package com.example.app.user.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="role")
public class UserRole {
    public enum RoleType {
        ADMIN,
        CONTENT_MANAGER,
        MARKETING_MANAGER
    }

    @Id
    @GeneratedValue
    @Column(name="id", nullable = false, unique = true)
    private Integer id;

    @Column(name="role", nullable = false)
    private RoleType role;

    @OneToMany(mappedBy = "role")
    private Set<ApplicationUser> users;
}
