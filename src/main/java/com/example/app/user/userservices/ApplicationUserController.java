package com.example.app.user.userservices;

import com.example.app.security.util.JWTTokenProcessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.example.app.security.configuration.SecurityConstants.HEADER_AUTHORIZATION_STRING;
import static com.example.app.security.configuration.SecurityConstants.TOKEN_PREFIX;

@RestController
public class ApplicationUserController {
    @PostMapping
    @RequestMapping("/refresh")
    public ResponseEntity<String> refreshToken(@RequestHeader(HEADER_AUTHORIZATION_STRING) String token) throws JsonProcessingException {
        if (token != null) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set(HEADER_AUTHORIZATION_STRING, TOKEN_PREFIX + new JWTTokenProcessor().update(token));
            return ResponseEntity.ok().headers(responseHeaders).build();
        }
        return ResponseEntity.badRequest().body("Missing \"Authorization\" header");
    }
}