package com.example.app.security.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;

import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.example.app.security.configuration.SecurityConstants.*;

public class JWTTokenProcessor {
    public final String SECRET = "SecretKeyToGenJWTs";
    public final long EXPIRATION_TIME = 3_600_000; // time in millisecs

    private String generateJWTToken(String username, Date date) {
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(date)
                .sign(HMAC512(SECRET.getBytes()));
    }

    private Long generateFreshTimestamp(Long time, int diff) {
        return (time - System.currentTimeMillis() > 2_000) ? time :
                generateFreshTimestamp(time + diff, diff + 150);
    }

    public String create(String username) {
        return generateJWTToken(
                username,
                new Date(System.currentTimeMillis() + EXPIRATION_TIME)
        );
    }

    public String update(String auth) throws JsonProcessingException {
        String base64EncodedBody = auth.split("\\.")[1];
        String body = new String(new Base64(true).decode(base64EncodedBody));
        JsonNode jsonNode = new ObjectMapper().readTree(body);

        return generateJWTToken(
                jsonNode.get("sub").asText(),
                new Date(generateFreshTimestamp(System.currentTimeMillis(), 150) + EXPIRATION_TIME)
        );
    }

    public String check(String token) {
        return JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getSubject();
    }
}
