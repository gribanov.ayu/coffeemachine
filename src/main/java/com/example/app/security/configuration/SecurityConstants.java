package com.example.app.security.configuration;

public class SecurityConstants {
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_AUTHORIZATION_STRING = "Authorization";
}
